﻿namespace Codefarts.UnityExtensionMethods
{
    using System;

    using UnityEngine;

    public class ColorHelpers
    {
        /// <summary>
        /// Blends two colors together.
        /// </summary>
        /// <param name="sourceColor">
        /// The source color.
        /// </param>
        /// <param name="blendColor">
        /// The blend color.
        /// </param>
        /// <returns>
        /// Returns the result as a <see cref="Color"/> type.
        /// </returns>
        /// <remarks>
        /// From Wikipedia https://en.wikipedia.org/wiki/Alpha_compositing -> "Alpha blending" 
        /// </remarks>
        public static Color Blend(Color sourceColor, Color blendColor)
        {
            var sr = blendColor.r / 255f;
            var sg = blendColor.g / 255f;
            var sb = blendColor.b / 255f;
            var sa = blendColor.a / 255f;
            var dr = sourceColor.r / 255f;
            var dg = sourceColor.g / 255f;
            var db = sourceColor.b / 255f;
            var da = sourceColor.a / 255f;

            var oa = sa + (da * (1 - sa));
            var r = ((sr * sa) + ((dr * da) * (1 - sa))) / oa;
            var g = ((sg * sa) + ((dg * da) * (1 - sa))) / oa;
            var b = ((sb * sa) + ((db * da) * (1 - sa))) / oa;
            var a = oa;

            // if alpha is 0 just return source color else build new blended color
            return Math.Abs(a - 0) < Mathf.Epsilon ? sourceColor : new Color((byte)(r * 255), (byte)(g * 255), (byte)(b * 255), (byte)(a * 255));
        }


        public static Color MakeColor(Vector3 hsb)
        {
            return MakeColor(new Vector4(hsb.x, hsb.y, hsb.z, 1.0f));
        }

        public static Color MakeColor(Vector4 hsba)
        {
            // When saturation = 0, then r, g, b represent grey value (= brightness (z)).
            var r = hsba.z;
            var g = hsba.z;
            var b = hsba.z;
            if (hsba.y > 0.0f)
            {  // saturation > 0
                // Calc sector
                var secPos = (hsba.x * 360.0f) / 60.0f;
                var secNr = Mathf.FloorToInt(secPos);
                var secPortion = secPos - secNr;

                // Calc axes p, q and t
                var p = hsba.z * (1.0f - hsba.y);
                var q = hsba.z * (1.0f - (hsba.y * secPortion));
                var t = hsba.z * (1.0f - (hsba.y * (1.0f - secPortion)));

                // Calc rgb
                if (secNr == 1)
                {
                    r = q;
                    g = hsba.z;
                    b = p;
                }
                else if (secNr == 2)
                {
                    r = p;
                    g = hsba.z;
                    b = t;
                }
                else if (secNr == 3)
                {
                    r = p;
                    g = q;
                    b = hsba.z;
                }
                else if (secNr == 4)
                {
                    r = t;
                    g = p;
                    b = hsba.z;
                }
                else if (secNr == 5)
                {
                    r = hsba.z;
                    g = p;
                    b = q;
                }
                else
                {
                    r = hsba.z;
                    g = t;
                    b = p;
                }
            }

            return new Color(r, g, b, hsba.w);
        }
    }
}
