﻿namespace Codefarts.UnityExtensionMethods
{
    using UnityEngine;

    /// <summary>
    /// Provides extension methods for the unity <see cref="Component"/> type.
    /// </summary>
    public static class ComponentExtensions
    {
        /// <summary>
        /// Gets or add a component. 
        /// </summary>
        /// <remarks>
        /// <example>
        /// Usage example:
        /// <code>
        /// var boxCollider = transform.GetOrAddComponent<BoxCollider>();
        /// </code>
        /// </example></remarks>
        static public T GetOrAddComponent<T>(this Component child) where T : Component
        {
            var result = child.GetComponent<T>();
            if (result == null)
            {
                result = child.gameObject.AddComponent<T>();
            }

            return result;
        }
    }
}
