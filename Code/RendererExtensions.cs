﻿namespace Codefarts.UnityExtensionMethods
{
    using UnityEngine;

    /// <summary>
    /// Provides extension methods for the unity <see cref="Renderer"/> component.
    /// </summary>
    public static class RendererExtensions
    {
        /// <summary>
        /// Determines whether the renderer is visible from the specified camera.
        /// </summary>
        /// <param name="renderer">The renderer to check for visibility.</param>
        /// <param name="camera">The camera to check against.</param>
        /// <returns>true if the renderer is visible to the camera; otherwise false.</returns>
        public static bool IsVisibleFrom(this Renderer renderer, Camera camera)
        {
            var planes = GeometryUtility.CalculateFrustumPlanes(camera);
            return GeometryUtility.TestPlanesAABB(planes, renderer.bounds);
        }
    }
}
