﻿namespace Codefarts.UnityExtensionMethods
{
    using System;

    using UnityEngine;

    /// <summary>
    /// Provides extension methods for the <see cref="Texture2D"/> type.
    /// </summary>
    public static class Texture2DExtensionMethods
    {
        /// <summary>
        /// Draws a <see cref="Texture2D"/> on to a <see cref="Texture2D"/> texture.
        /// </summary>
        /// <param name="texture">A reference to a <see cref="Texture2D"/> type.</param>
        /// <param name="sourceImage">A reference to the <see cref="Texture2D"/> that will be drawn.</param>
        /// <param name="sourceRectangle">The source rectangle within the <see cref="sourceImage"/> that will be drawn.</param>
        /// <param name="position">The position within <see cref="texture"/> where the <see cref="sourceImage"/> will be drawn at.</param>
        public static void Draw(this Texture2D texture, Texture2D sourceImage, Rect sourceRectangle, Vector2 position)
        {
            Draw(texture, sourceImage, (int)position.x, (int)position.y, (int)sourceRectangle.x, (int)sourceRectangle.y, (int)sourceRectangle.width, (int)sourceRectangle.height, false, false);
        }         

        /// <summary>
        /// Draws a <see cref="Texture2D"/> on to a <see cref="Texture2D"/> texture.
        /// </summary>
        /// <param name="texture">A reference to a <see cref="Texture2D"/> type.</param>
        /// <param name="sourceImage">A reference to the <see cref="Texture2D"/> that will be drawn.</param>
        /// <param name="sourceRectangle">The source rectangle within the <see cref="sourceImage"/> that will be drawn.</param>
        /// <param name="position">The position within <see cref="texture"/> where the <see cref="sourceImage"/> will be drawn at.</param>
        /// <param name="flipHorizontally">If true will flip the <see cref="sourceImage"/> horizontally before drawing.</param>
        /// <param name="flipVertically">If true will flip the <see cref="sourceImage"/> vertically before drawing.</param>
        public static void Draw(this Texture2D texture, Texture2D sourceImage, Rect sourceRectangle, Vector2 position, bool flipHorizontally, bool flipVertically)
        {
            Draw(texture, sourceImage, (int)position.x, (int)position.y, (int)sourceRectangle.x, (int)sourceRectangle.y, (int)sourceRectangle.width, (int)sourceRectangle.height, flipHorizontally, flipVertically);
        }

        /// <summary>
        /// Draws a <see cref="Texture2D"/> on to a <see cref="Texture2D"/> texture.
        /// </summary>
        /// <param name="texture">A reference to a <see cref="Texture2D"/> type.</param>
        /// <param name="sourceImage">A reference to the <see cref="Texture2D"/> that will be drawn.</param>
        /// <param name="x">The x position where the <see cref="sourceImage"/> will be drawn.</param>
        /// <param name="y">The y position where the <see cref="sourceImage"/> will be drawn.</param>
        /// <param name="sourceX">The source x position within <see cref="sourceImage"/>.</param>
        /// <param name="sourceY">The source y position within <see cref="sourceImage"/>.</param>
        /// <param name="sourceWidth">The source width within the <see cref="sourceImage"/>.</param>
        /// <param name="sourceHeight">The source height within the <see cref="sourceImage"/>.</param>
        /// <param name="flipHorizontally">If true will flip the <see cref="sourceImage"/> horizontally before drawing.</param>
        /// <param name="flipVertically">If true will flip the <see cref="sourceImage"/> vertically before drawing.</param>
        public static void Draw(this Texture2D texture, Texture2D sourceImage, int x, int y, int sourceX, int sourceY, int sourceWidth, int sourceHeight, bool flipHorizontally, bool flipVertically)
        {
            var textureRectangle = new Rect(0, 0, texture.width, texture.height);
            var sourceRectangle = new Rect(x, y, sourceWidth, sourceHeight);
            var intersect = textureRectangle.Intersect(sourceRectangle);

            if (!intersect.Intersects(new Rect(0, 0, sourceImage.width, sourceImage.height)))
            {
                return;
            }

            var tempImage = new Texture2D((int)intersect.width, (int)intersect.height);
            tempImage.Draw(sourceImage, 0, 0, sourceX, sourceY, tempImage.width, tempImage.height, (source, blendWith) => blendWith);

            if (flipHorizontally)
            {
                tempImage.FlipHorizontally();
            }

            if (flipVertically)
            {
                tempImage.FlipVertically();
            }

            var colors = tempImage.GetPixels();
            texture.SetPixels(x, y, (int)intersect.width, (int)intersect.height, colors);
            texture.Apply();
        }

        /// <summary>
        /// Flips a image horizontally.
        /// </summary>             
        /// <param name="image">A reference to the image to be flipped.</param>
        /// <exception cref="NullReferenceException">If the <see cref="image"/> parameter is null.</exception>
        public static void FlipHorizontally(this Texture2D image)
        {
            var pixels = image.GetPixels32();
            var newPixels = new Color32[pixels.Length];

            for (var y = 0; y < image.height; y++)
            {
                for (var x = 0; x < image.width; x++)
                {
                    var newIndex = (y * image.width) + x;
                    var pixelsIndex = (y * image.width) + (image.width - 1 - x);
                    newPixels[newIndex] = pixels[pixelsIndex];
                }
            }

            image.SetPixels32(newPixels);
            image.Apply();
        }

        /// <summary>
        /// Flips a image vertically.
        /// </summary>       
        /// <param name="image">A reference to the image to be flipped.</param>
        /// <exception cref="NullReferenceException">If the <see cref="image"/> parameter is null.</exception>
        public static void FlipVertically(this Texture2D image)
        {
            var pixels = image.GetPixels32();
            var newPixels = new Color32[pixels.Length];

            for (var y = 0; y < image.height; y++)
            {
                for (var x = 0; x < image.width; x++)
                {
                    var newIndex = (y * image.width) + x;
                    var pixelsIndex = ((image.height - 1 - y) * image.width) + x;
                    newPixels[newIndex] = pixels[pixelsIndex];
                }
            }

            image.SetPixels32(newPixels);
            image.Apply();
        }

        /// <summary>
        /// Draws a image within another image.
        /// </summary>            
        /// <param name="image">
        /// The destination image.
        /// </param>
        /// <param name="source">
        /// The source image.
        /// </param>
        /// <param name="x">
        /// The x position in the destination image.
        /// </param>
        /// <param name="y">
        /// The y position in the destination image.
        /// </param>
        /// <param name="blendCallback">
        /// A <see cref="Func{TResult}"/> callback that is used to perform pixel blending.
        /// </param>                     
        public static void Draw(this Texture2D image, Texture2D source, int x, int y, Func<Color, Color, Color> blendCallback)
        {
            Draw(image, source, x, y, source.width, source.height, 0, 0, source.width, source.height, blendCallback);
        }

        /// <summary>
        /// Draws a image within another image.
        /// </summary>           
        /// <param name="image">
        /// The destination image.
        /// </param>
        /// <param name="source">
        /// The source image.
        /// </param>
        /// <param name="x">
        /// The x position in the destination image.
        /// </param>
        /// <param name="y">
        /// The y position in the destination image.
        /// </param>                
        public static void Draw(this Texture2D image, Texture2D source, int x, int y)
        {
            Draw(image, source, x, y, source.width, source.height, 0, 0, source.width, source.height, (destinationColor, sourceColor) => sourceColor);
        }

        /// <summary>
        /// Draws a image within another image.
        /// </summary>            
        /// <param name="image">
        /// The destination image.
        /// </param>
        /// <param name="source">
        /// The source image.
        /// </param>
        /// <param name="x">
        /// The x position in the destination image.
        /// </param>
        /// <param name="y">
        /// The y position in the destination image.
        /// </param>
        /// <param name="width">
        /// The destination width of the drawn image.
        /// </param>
        /// <param name="height">
        /// The destination height of the drawn image.
        /// </param>
        /// <param name="blendCallback">
        /// A <see cref="Func{TResult}"/> callback that is used to perform pixel blending.
        /// </param>                                               
        public static void Draw(this Texture2D image, Texture2D source, int x, int y, int width, int height, Func<Color, Color, Color> blendCallback)
        {
            Draw(image, source, x, y, width, height, 0, 0, source.width, source.height, blendCallback);
        }

        /// <summary>
        /// Draws a image within another image.
        /// </summary>          
        /// <param name="image">
        /// The destination image.
        /// </param>
        /// <param name="source">
        /// The source image.
        /// </param>
        /// <param name="x">
        /// The x position in the destination image.
        /// </param>
        /// <param name="y">
        /// The y position in the destination image.
        /// </param>
        /// <param name="sourceX">
        /// The x position in the source image.
        /// </param>
        /// <param name="sourceY">
        /// The y position in the source image.
        /// </param>
        /// <param name="sourceWidth">
        /// The source width.
        /// </param>
        /// <param name="sourceHeight">
        /// The source height.
        /// </param>
        /// <param name="blendCallback">
        /// A <see cref="Func{TResult}"/> callback that is used to perform pixel blending.
        /// </param>                  
        public static void Draw(this Texture2D image, Texture2D source, int x, int y, int sourceX, int sourceY, int sourceWidth, int sourceHeight, Func<Color, Color, Color> blendCallback)
        {
            Draw(image, source, x, y, sourceWidth, sourceHeight, sourceX, sourceY, sourceWidth, sourceHeight, blendCallback);
        }

        /// <summary>
        /// Draws a image within another image.
        /// </summary>       
        /// <param name="image">
        /// The destination image.
        /// </param>
        /// <param name="source">
        /// The source image.
        /// </param>
        /// <param name="x">
        /// The x position in the destination image.
        /// </param>
        /// <param name="y">
        /// The y position in the destination image.
        /// </param>
        /// <param name="width">
        /// The destination width of the drawn image.
        /// </param>
        /// <param name="height">
        /// The destination height of the drawn image.
        /// </param>
        /// <param name="sourceX">
        /// The x position in the source image.
        /// </param>
        /// <param name="sourceY">
        /// The y position in the source image.
        /// </param>
        /// <param name="sourceWidth">
        /// The source width.
        /// </param>
        /// <param name="sourceHeight">
        /// The source height.
        /// </param>
        /// <param name="blendCallback">
        /// A <see cref="Func{TResult}"/> callback that is used to perform pixel blending.
        /// </param>
        /// <exception cref="ArgumentNullException">
        /// If <see cref="source"/> or <see cref="blendCallback"/> is null.
        /// </exception>
        /// <exception cref="ArgumentOutOfRangeException">
        /// If width, height, sourceWidth or sourceHeight are less then 1.
        /// </exception>                                           
        public static void Draw(this Texture2D image, Texture2D source, int x, int y, int width, int height, int sourceX, int sourceY, int sourceWidth, int sourceHeight, Func<Color, Color, Color> blendCallback)
        {
            // perform input validation
            if (source == null)
            {
                throw new ArgumentNullException("source");
            }

            if (blendCallback == null)
            {
                throw new ArgumentNullException("blendCallback");
            }

            if (sourceWidth < 1)
            {
                throw new ArgumentOutOfRangeException("sourceWidth");
            }

            if (sourceHeight < 1)
            {
                throw new ArgumentOutOfRangeException("sourceHeight");
            }

            if (width < 1)
            {
                throw new ArgumentOutOfRangeException("width");
            }

            if (height < 1)
            {
                throw new ArgumentOutOfRangeException("height");
            }

            var scaledWidth = (float)width / sourceWidth;
            var scaledHeight = (float)height / sourceHeight;

            if (x > image.width - 1 || y > image.height - 1 || sourceX > source.width - 1 || sourceY > source.height - 1)
            {
                return;
            }

            // copy source data to temp image
            var temporaryImage = new Texture2D(sourceWidth, sourceHeight, source.format, source.mipmapCount > 1);
            var tempPixels = temporaryImage.GetPixels32();
            var sourcePixels = source.GetPixels32();

            for (var indexY = 0; indexY < sourceHeight; indexY++)
            {
                for (var indexX = 0; indexX < sourceWidth; indexX++)
                {
                    var positionX = indexX + sourceX;
                    var positionY = indexY + sourceY;

                    // TODO: this needs to be better optimized. We are processing all pixels even though some of 
                    // the pixels could be cropped off. Need to calculate the rectangle the copy pixels and process only that rectangle
                    if (positionX > source.width - 1 || positionY > source.height - 1 || positionX < 0 || positionY < 0)
                    {
                        continue;
                    }

                    var sourceIndex = (positionY * source.width) + positionX;
                    var tempIndex = (indexY * temporaryImage.width) + indexX;
                    tempPixels[tempIndex] = sourcePixels[sourceIndex];
                }
            }

            temporaryImage.SetPixels32(tempPixels);
            temporaryImage.Apply();

            var scaled = temporaryImage.Scale(scaledWidth, scaledHeight);
            var scaledPixels = scaled.GetPixels32();
            var imagePixels = image.GetPixels32();

            if (x < -scaled.width - 1 || y < -scaled.height - 1)
            {
                return;
            }

            for (var indexY = 0; indexY < scaled.height; indexY++)
            {
                for (var indexX = 0; indexX < scaled.width; indexX++)
                {
                    var destinationX = indexX + x;
                    var destinationY = indexY + y;

                    // TODO: this needs to be better optimized. We are processing all pixels even though some of 
                    // the pixels could be cropped off. Need to calculate the rectangle the copy pixels and process only that rectangle
                    if (destinationX > image.width - 1 || destinationY > image.height - 1 || destinationX < 0 || destinationY < 0)
                    {
                        continue;
                    }

                    var scaledIndex = (indexY * scaled.width) + indexX;
                    var value = scaledPixels[scaledIndex];

                    var destinationIndex = (destinationY * image.width) + destinationX;
                    imagePixels[destinationIndex] = blendCallback(imagePixels[destinationIndex], value);
                }
            }

            image.SetPixels32(imagePixels);
            image.Apply();
        }

        /// <summary>
        /// Scales a image.
        /// </summary>
        /// <param name="image">
        /// The source image.
        /// </param>
        /// <param name="x">
        /// The horizontal scale.
        /// </param>
        /// <param name="y">
        /// The vertical scale.
        /// </param>
        /// <returns>
        /// Returns a new scaled image.
        /// </returns>
        /// <exception cref="ArgumentOutOfRangeException">
        /// x and y values must be greater then 0.
        /// </exception>
        public static Texture2D Scale(this Texture2D image, float x, float y)
        {
            if (x <= 0)
            {
                throw new ArgumentOutOfRangeException("x");
            }

            if (y <= 0)
            {
                throw new ArgumentOutOfRangeException("y");
            }

            if (Math.Abs(x - 1.0f) < 0 && Math.Abs(y - 1.0f) < 0)
            {
                return image.Clone();
            }

            var width = (int)(image.width * x);
            var height = (int)(image.height * y);
            if (width < 1)
            {
                width = 1;
            }

            if (height < 1)
            {
                height = 1;
            }

            var scaled = new Texture2D(width, height, image.format, image.mipmapCount > 1);
            var pixels = scaled.GetPixels32();
            var imagePixels = image.GetPixels32();

            for (var indexY = 0; indexY < scaled.height; indexY++)
            {
                for (var indexX = 0; indexX < scaled.width; indexX++)
                {
                    var u = scaled.width == 1 ? 1 : indexX / (float)(scaled.width - 1);
                    var v = scaled.height == 1 ? 1 : indexY / (float)(scaled.height - 1);
                    var scaledIndex = (indexY * scaled.width) + indexX;
                    var imageIndex = ((int)Math.Round(v * (image.height - 1)) * image.width) + (int)Math.Round(u * (image.width - 1));
                    pixels[scaledIndex] = imagePixels[imageIndex];
                }
            }

            scaled.SetPixels32(pixels);
            scaled.Apply();

            return scaled;
        }

        /// <summary>
        /// Clones an texture.
        /// </summary>
        /// <param name="image">
        /// The source image.
        /// </param>
        /// <returns>
        /// Returns a new image containing the same data.
        /// </returns>
        public static Texture2D Clone(this Texture2D image)
        {
            var newImage = new Texture2D(image.width, image.height, image.format, image.mipmapCount > 1);
            var pixels = image.GetPixels32();
            newImage.SetPixels32(pixels);
            newImage.Apply();
            return newImage;
        }

        /// <summary>
        /// Draws a line on an <see cref="Texture2D"/>.
        /// </summary>          
        /// <param name="image">
        /// The destination image.
        /// </param>
        /// <param name="x1">
        /// The x position for the start of the line.
        /// </param>
        /// <param name="y1">
        /// The y position for the start of the line.
        /// </param>
        /// <param name="x2">
        /// The x position for the end of the line.
        /// </param>
        /// <param name="y2">
        /// The y position for the end of the line.
        /// </param>
        /// <param name="value">
        /// The value that the line will be drawn with.
        /// </param>                                              
        public static void DrawLine(this Texture2D image, int x1, int y1, int x2, int y2, Color value)
        {
            var pixels = image.GetPixels32();
            
            // source (converted from C) -> http://rosettacode.org/wiki/Bitmap/Bresenham%27s_line_algorithm#C
            int dx = Math.Abs(x2 - x1);
            int dy = Math.Abs(y2 - y1);
            int sx = x1 < x2 ? 1 : -1;
            int sy = y1 < y2 ? 1 : -1;
            int err = (dx > dy ? dx : -dy) / 2;
            int e2;

            while (true)
            {
                var index = (y1 * image.width) + x1;
                pixels[index] = value;
                if (x1 == x2 && y1 == y2)
                {
                    break;
                }

                e2 = err;
                if (e2 > -dx)
                {
                    err -= dy;
                    x1 += sx;
                }

                if (e2 < dy)
                {
                    err += dx;
                    y1 += sy;
                }
            }

            image.SetPixels32(pixels);
            image.Apply();
        }

        /// <summary>
        /// Draws a filled Ellipse.
        /// </summary>                 
        /// <param name="image">
        /// The destination image.
        /// </param>
        /// <param name="x">
        /// The x left most position of the ellipse.
        /// </param>
        /// <param name="y">
        /// The y top most position of the ellipse.
        /// </param>
        /// <param name="width">
        /// The width of the ellipse.
        /// </param>
        /// <param name="height">
        /// The height of the ellipse.
        /// </param>
        /// <param name="color">
        /// The value to use.
        /// </param>               
        public static void FillEllipse(this Texture2D image, int x, int y, int width, int height, Color color)
        {
            var pixels = image.GetPixels32();
            width = width / 2;
            height = height / 2;
            var centerX = x + width;
            var centerY = y + height;

            // source -> http://stackoverflow.com/questions/10322341/simple-algorithm-for-drawing-filled-ellipse-in-c-c
            for (var indexY = -height; indexY <= height; indexY++)
            {
                for (var indexX = -width; indexX <= width; indexX++)
                {
                    var dx = indexX / (double)width;
                    var dy = indexY / (double)height;
                    if ((dx * dx) + (dy * dy) <= 1)
                    {
                        var index = ((centerY + indexY) * image.width) + centerX + indexX;
                        pixels[index] = color;
                    }
                }
            }

            image.SetPixels32(pixels);
            image.Apply();
        }

        /// <summary>
        /// Draws a Ellipse.
        /// </summary>
        /// <param name="image">
        /// The destination image.
        /// </param>
        /// <param name="x">
        /// The x center position of the circle.
        /// </param>
        /// <param name="y">
        /// The y center position of the circle.
        /// </param>
        /// <param name="width">
        /// The width of the Ellipse.
        /// </param>
        /// <param name="height">
        /// The height of the Ellipse.
        /// </param>
        /// <param name="color">
        /// The value to use.
        /// </param>
        public static void DrawEllipse(this Texture2D image, int x, int y, int width, int height, Color color)
        {
            // source (converted from C++) -> http://www.dailyfreecode.com/Code/draw-ellipse-midpoint-ellipse-algorithm-714.aspx
            var radiusX = width / 2f;
            var radiusY = height / 2f;
            float aa = radiusX * radiusX;
            float bb = radiusY * radiusY;
            float aa2 = aa * 2;
            float bb2 = bb * 2;

            float positionX = 0;
            float positionY = radiusY;

            float fx = 0;
            float fy = aa2 * radiusY;

            float p = (int)(bb - (aa * radiusY) + (0.25 * aa) + 0.5);
            var centerX = x + radiusX;
            var centerY = y + radiusY;

            var pixels = image.GetPixels32();
            pixels[((int)(centerY + positionY) * image.width) + (int)(centerX + positionX)] = color;
            pixels[((int)(centerY - positionY) * image.width) + (int)(centerX + positionX)] = color;
            pixels[((int)(centerY - positionY) * image.width) + (int)(centerX - positionX)] = color;
            pixels[((int)(centerY + positionY) * image.width) + (int)(centerX - positionX)] = color;

            while (fx < fy)
            {
                positionX++;
                fx += bb2;

                if (p < 0)
                {
                    p += fx + bb;
                }
                else
                {
                    positionY--;
                    fy -= aa2;
                    p += fx + bb - fy;
                }

                pixels[((int)(centerY + positionY) * image.width) + (int)(centerX + positionX)] = color;
                pixels[((int)(centerY - positionY) * image.width) + (int)(centerX + positionX)] = color;
                pixels[((int)(centerY - positionY) * image.width) + (int)(centerX - positionX)] = color;
                pixels[((int)(centerY + positionY) * image.width) + (int)(centerX - positionX)] = color;
            }

            p = (int)((bb * (positionX + 0.5) * (positionX + 0.5)) + (aa * (positionY - 1) * (positionY - 1)) - (aa * bb) + 0.5);

            while (positionY > 0)
            {
                positionY--;
                fy -= aa2;

                if (p >= 0)
                {
                    p += aa - fy;
                }
                else
                {
                    positionX++;
                    fx += bb2;
                    p += fx + aa - fy;
                }

                pixels[((int)(centerY + positionY) * image.width) + (int)(centerX + positionX)] = color;
                pixels[((int)(centerY - positionY) * image.width) + (int)(centerX + positionX)] = color;
                pixels[((int)(centerY - positionY) * image.width) + (int)(centerX - positionX)] = color;
                pixels[((int)(centerY + positionY) * image.width) + (int)(centerX - positionX)] = color;
            }

            image.SetPixels32(pixels);
            image.Apply();
        }

        /// <summary>
        /// Draws a filled rectangle.
        /// </summary>
        /// <param name="image">
        /// The destination image.
        /// </param>
        /// <param name="x">
        /// The x position of the left side of the rectangle.
        /// </param>
        /// <param name="y">
        /// The y position of the top side of the rectangle.
        /// </param>
        /// <param name="width">
        /// The width of the rectangle.
        /// </param>
        /// <param name="height">
        /// The height of the rectangle.
        /// </param>
        /// <param name="color">
        /// The value to use.
        /// </param>                                                 
        public static void FillRectangle(this Texture2D image, int x, int y, int width, int height, Color color)
        {
            var pixels = image.GetPixels32();
            for (var indexX = 0; indexX < width; indexX++)
            {
                for (var indexY = 0; indexY < height; indexY++)
                {
                    var positionX = indexX + x;
                    var positionY = indexY + y;
                    if (positionX > image.width || positionY > image.height || positionX < 0 || positionY < 0)
                    {
                        continue;
                    }

                    var index = (positionY * image.width) + positionX;
                    pixels[index] = ColorHelpers.Blend(pixels[index], color);
                }
            }

            image.SetPixels32(pixels);
            image.Apply();
        }

        /// <summary>
        /// Draws a rectangle.
        /// </summary>
        /// <param name="image">
        /// The destination image.
        /// </param>
        /// <param name="x">
        /// The x position of the left side of the rectangle.
        /// </param>
        /// <param name="y">
        /// The y position of the top side of the rectangle.
        /// </param>
        /// <param name="width">
        /// The width of the rectangle.
        /// </param>
        /// <param name="height">
        /// The height of the rectangle.
        /// </param>
        /// <param name="value">
        /// The value to use.
        /// </param>   
        public static void DrawRectangle(this Texture2D image, int x, int y, int width, int height, Color value)
        {
            DrawRectangle(image, x, y, width, height, value, ColorHelpers.Blend);
        }

        /// <summary>
        /// Draws a rectangle.
        /// </summary>        
        /// <param name="image">
        /// The destination image.
        /// </param>
        /// <param name="x">
        /// The x position of the left side of the rectangle.
        /// </param>
        /// <param name="y">
        /// The y position of the top side of the rectangle.
        /// </param>
        /// <param name="width">
        /// The width of the rectangle.
        /// </param>
        /// <param name="height">
        /// The height of the rectangle.
        /// </param>
        /// <param name="value">
        /// The value to use.
        /// </param>
        /// <param name="blendCallback">
        /// A <see cref="Func{TResult}"/> callback that is used to perform pixel blending.
        /// </param>
        /// <exception cref="ArgumentNullException">
        /// If <see cref="blendCallback"/> is null.
        /// </exception>
        public static void DrawRectangle(this Texture2D image, int x, int y, int width, int height, Color value, Func<Color, Color, Color> blendCallback)
        {
            if (blendCallback == null)
            {
                throw new ArgumentNullException("blendCallback");
            }

            var pixels = image.GetPixels32();

            var bottom = y + height;
            var right = x + width;

            // allows negative widths
            if (x > right)
            {
                var temp = right;
                right = x;
                x = temp;
            }

            // allows negative heights
            if (y > bottom)
            {
                var temp = bottom;
                bottom = y;
                y = temp;
            }

            // top 
            if (y > -1 && y < image.height)
            {
                var leftValue = x < 0 ? 0 : x;
                var rightValue = right > image.width - 1 ? image.width - 1 : right;
                for (var i = leftValue; i <= rightValue; i++)
                {
                    var index = (y * image.width) + i;
                    pixels[index] = blendCallback(pixels[index], value);
                }
            }

            // bottom 
            if (bottom > -1 && bottom < image.height)
            {
                var leftValue = x < 0 ? 0 : x;
                var rightValue = right > image.width - 1 ? image.width - 1 : right;
                for (var i = leftValue; i <= rightValue; i++)
                {
                    var index = (bottom * image.width) + i;
                    pixels[index] = blendCallback(pixels[index], value);
                }
            }

            // left 
            if (x > -1 && x < image.width)
            {
                var topValue = y < 0 ? 0 : y;
                var bottomValue = bottom > image.height - 1 ? image.height - 1 : bottom;
                for (var i = topValue; i <= bottomValue; i++)
                {
                    var index = (i * image.width) + x;
                    pixels[index] = blendCallback(pixels[index], value);
                }
            }

            // right 
            if (right > -1 && right < image.width)
            {
                var topValue = y < 0 ? 0 : y;
                var bottomValue = bottom > image.height - 1 ? image.height - 1 : bottom;
                for (var i = topValue; i <= bottomValue; i++)
                {
                    var index = (i * image.width) + right;
                    pixels[index] = blendCallback(pixels[index], value);
                }
            }

            image.SetPixels32(pixels);
            image.Apply();
        }
    }
}
