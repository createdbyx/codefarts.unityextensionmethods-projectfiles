﻿namespace Codefarts.UnityExtensionMethods
{
    using UnityEngine;

    /// <summary>
    /// Provides helper methods for the unity Texture2D class.
    /// </summary>
    public class Texture2DHelpers
    {
        /// <summary>
        /// Gets a new empty texture consisting of a specified color.
        /// </summary>
        /// <param name="w">The texture width.</param>
        /// <param name="h">The texture height.</param>
        /// <param name="color">The solid uniform color that the texture will have.</param>
        /// <returns>A new RGBA32 <see cref="Texture2D"/> consisting of a single color.</returns>
        public static Texture2D GetEmptyTexture(int w, int h, Color color)
        {
            var img = new Texture2D(w, h, TextureFormat.RGBA32, false);
            var pixels = img.GetPixels(0);
            for (var i = 0; i < pixels.Length; i++)
            {
                pixels[i] = color;
            }

            img.SetPixels(pixels, 0);
            img.Apply();
            
            return img;
        }

        public static Texture2D GetHSTexture(int width, int height, float brightness)
        {
            var img = new Texture2D(width, height, TextureFormat.RGBA32, false);
            for (var y = 0; y < height; y++)
            {
                for (var x = 0; x < width; x++)
                {
                    var p = MakeColor(new Vector3((float)x / width, (float)y / height, brightness));
                    img.SetPixel(x, y, p);
                }
            }

            img.Apply();
            return img;
        }
        public static Texture2D GetHBTexture(int width, int height, float saturation)
        {
            Texture2D img = new Texture2D(width, height, TextureFormat.RGBA32, false);
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    Color p = MakeColor(new Vector3((float)x / width, saturation, (float)y / height));
                    img.SetPixel(x, y, p);
                }
            }
            img.Apply();
            return img;
        }
        public static Texture2D GetSBTexture(int width, int height, float hue)
        {
            Texture2D img = new Texture2D(width, height, TextureFormat.RGBA32, false);
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    Color p = MakeColor(new Vector3(hue, (float)x / width, (float)y / height));
                    img.SetPixel(x, y, p);
                }
            }
            img.Apply();
            return img;
        }

        public static Texture2D GetHTexture(int width, int height, float saturation, float brightness)
        {
            Texture2D img = new Texture2D(width, height, TextureFormat.RGBA32, false);
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    Color p = (width > height)
                        ? MakeColor(new Vector3((float)x / width, saturation, brightness))
                        : MakeColor(new Vector3((float)y / height, saturation, brightness));
                    img.SetPixel(x, y, p);
                }
            }
            img.Apply();
            return img;
        }
        public static Texture2D GetSTexture(int width, int height, float hue, float brightness)
        {
            Texture2D img = new Texture2D(width, height, TextureFormat.RGBA32, false);
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    Color p = (width > height)
                        ? MakeColor(new Vector3(hue, (float)x / width, brightness))
                        : MakeColor(new Vector3(hue, (float)y / height, brightness));
                    img.SetPixel(x, y, p);
                }
            }
            img.Apply();
            return img;
        }
        public static Texture2D GetBTexture(int width, int height, float hue, float saturation)
        {
            Texture2D img = new Texture2D(width, height, TextureFormat.RGBA32, false);
            
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    Color p = (width > height)
                        ? MakeColor(new Vector3(hue, saturation, (float)x / width))
                        : MakeColor(new Vector3(hue, saturation, (float)y / height));
                    img.SetPixel(x, y, p);
                }
            }
            img.Apply();
            return img;
        }

        public static Texture2D GetATexture(int width, int height, Color color)
        {
            Texture2D img = new Texture2D(width, height, TextureFormat.RGBA32, false);
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    Color p = color;
                    p.a = (width > height)
                        ? (float)x / width
                        : (float)y / height;
                    img.SetPixel(x, y, p);
                }
            }
            img.Apply();
            return img;
        }

        private static Color MakeColor(Vector3 hsb)
        {
            return MakeColor(new Vector4(hsb.x, hsb.y, hsb.z, 1.0f));
        }

        private static Color MakeColor(Vector4 hsba)
        {
            // When saturation = 0, then r, g, b represent grey value (= brightness (z)).
            var r = hsba.z;
            var g = hsba.z;
            var b = hsba.z;
            if (hsba.y > 0.0f)
            {  // saturation > 0
                // Calc sector
                var secPos = (hsba.x * 360.0f) / 60.0f;
                var secNr = Mathf.FloorToInt(secPos);
                var secPortion = secPos - secNr;

                // Calc axes p, q and t
                var p = hsba.z * (1.0f - hsba.y);
                var q = hsba.z * (1.0f - (hsba.y * secPortion));
                var t = hsba.z * (1.0f - (hsba.y * (1.0f - secPortion)));

                // Calc rgb
                if (secNr == 1)
                {
                    r = q;
                    g = hsba.z;
                    b = p;
                }
                else if (secNr == 2)
                {
                    r = p;
                    g = hsba.z;
                    b = t;
                }
                else if (secNr == 3)
                {
                    r = p;
                    g = q;
                    b = hsba.z;
                }
                else if (secNr == 4)
                {
                    r = t;
                    g = p;
                    b = hsba.z;
                }
                else if (secNr == 5)
                {
                    r = hsba.z;
                    g = p;
                    b = q;
                }
                else
                {
                    r = hsba.z;
                    g = t;
                    b = p;
                }
            }

            return new Color(r, g, b, hsba.w);
        }
    }
}
