﻿namespace Codefarts.UnityExtensionMethods.Scripts
{
    using System;

    using UnityEngine;

    public class TextureExtensionTests : MonoBehaviour
    {
        private Vector2 examplesScroll;
        private enum ExampleNames
        {
            SolidColorTexture = 0
        }

        private ExampleNames currentExample = ExampleNames.SolidColorTexture;

        /// <summary>
        /// OnGUI is called for rendering and handling GUI events.
        /// </summary>
        public void OnGUI()
        {
            using (var h = new GUILayout.HorizontalScope(GUILayout.ExpandHeight(true), GUILayout.ExpandWidth(true)))
            {
                this.DrawExampleList();
                this.DrawExample();
            }
        }

        private void DrawExample()
        {

        }

        private void DrawExampleList()
        {
            this.examplesScroll = GUILayout.BeginScrollView(this.examplesScroll, false, false, GUILayout.MaxWidth(200), GUILayout.Width(200), GUILayout.ExpandHeight(true));

            var names = Enum.GetNames(typeof(ExampleNames));
            for (var i = 0; i < names.Length; i++)
            {
                var match = i == (int)this.currentExample;
                if (GUILayout.Toggle(match, names[i], GUI.skin.button))
                {
                    this.currentExample = (ExampleNames)i;
                }
            }

            GUILayout.EndScrollView();
        }
    }
}